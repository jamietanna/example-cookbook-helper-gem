require 'foodcritic'
require 'knife_cookbook_doc/rake_task'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'

namespace :style do
  RuboCop::RakeTask.new(:rubocop)

  FoodCritic::Rake::LintTask.new(:foodcritic) do |t|
    # As with the FoodCritic CLI, fail if there are any tags that fail
    t.options = {
      fail_tags: ['any']
    }
  end
end

namespace :unit do
  RSpec::Core::RakeTask.new(:spec)
end

namespace :doc do
  KnifeCookbookDoc::RakeTask.new(:readme)

  KnifeCookbookDoc::RakeTask.new(:readme_test) do |t|
    t.options[:output_file] = 'tmp/README.md'
  end

  task :test_dir do
    FileUtils.mkdir_p 'tmp'
  end

  desc 'Verify that documentation has been generated from latest source code'
  task test: [:test_dir, :readme_test] do
    unless FileUtils.identical?('README.md', 'tmp/README.md')
      $stderr.puts "Generated file is not identical to the README.md in the repo. Please update it:"
      # the command will fail with an error code, therefore failing the Rake task
      sh 'diff -au --color README.md tmp/README.md'
    end
  end
end

task style: ['style:rubocop', 'style:foodcritic']
task unit: ['unit:spec']
task doc: ['doc:readme']
task default: ['style', 'doc:test', 'unit']
