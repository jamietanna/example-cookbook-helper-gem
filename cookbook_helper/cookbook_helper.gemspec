# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "cookbook_helper/version"

Gem::Specification.new do |spec|
  spec.name          = "cookbook_helper"
  spec.version       = CookbookHelper::VERSION
  spec.authors       = ["Jamie Tanna"]
  spec.email         = ["rubygems@jamietanna.co.uk"]
  spec.license       = 'Apache-2.0'

  spec.summary       = %q{TODO: Write a short summary, because Rubygems requires one.}
  spec.description   = %q{TODO: Write a longer description or delete this line.}
  spec.homepage      = "TODO: Put your gem's website or public repo URL here."

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"

  spec.add_runtime_dependency 'berkshelf', '= 6.3.1'
  spec.add_runtime_dependency 'chef', '= 13.6.4'
  spec.add_runtime_dependency 'chefspec', '= 7.1.1'
  spec.add_runtime_dependency 'foodcritic', '= 12.2.1'
  spec.add_runtime_dependency 'knife-cookbook-doc', '~> 0.25'
  spec.add_runtime_dependency 'rubocop', '= 0.49.1'
end
